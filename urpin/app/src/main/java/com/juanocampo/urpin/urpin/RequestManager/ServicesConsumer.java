package com.juanocampo.urpin.urpin.RequestManager;


/**
 * Created by juan.ocampo
 */
public interface ServicesConsumer {
    public void processResponse(ResponseFormat json);
}
