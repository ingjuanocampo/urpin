package com.juanocampo.urpin.urpin.RequestManager;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;


/**
 * Created by juan.ocampo
 */
public class PostMethod {

    public final static String urlbase = "http://runnerhitcombo-env.elasticbeanstalk.com/runner";


    private String url = urlbase + "/bars";


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    /**
     * Method in use
     * @return
     * @throws IOException
     */
    public String GetFromServerURL(String complement, Map<String, String> params, final Map<String, String> headers) throws IOException {


        URL mURL = new URL(urlbase + complement + createQueryStringForParameters(params));

        final HttpURLConnection urlConnection = (HttpURLConnection) mURL.openConnection();
        //urlConnection.setRequestProperty("Accept", "application/json");

        for (String key: headers.keySet() ){
            urlConnection.setRequestProperty(key, headers.get(key));
        }

        urlConnection.setRequestMethod("GET");

        Log.w("Data-OUT", urlConnection.getRequestProperties().toString());
        Log.w("Data-URL", urlbase + complement + createQueryStringForParameters(params));


        String jsonResultStr = "";
        try {
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());


            BufferedReader reader = new BufferedReader(new InputStreamReader(in));


            StringBuilder total = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                total.append(line);
            }

            jsonResultStr = total.toString();

            Log.w("Data-IN", jsonResultStr);


        } finally {
            urlConnection.disconnect();
        }


        return jsonResultStr;
    }


    /**
     * Method in use
     *
     * @return
     * @throws IOException
     */
    public String PostToServerURL(String complement, String json, Map<String, String> params, final Map<String, String> headers) throws IOException {


        URL mURL = new URL(urlbase + complement);

        final HttpURLConnection urlConnection = (HttpURLConnection) mURL.openConnection();
        urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");


        for (String key: headers.keySet() ){
            urlConnection.setRequestProperty(key, headers.get(key));
        }


        urlConnection.setRequestMethod("POST");
        Log.w("Data-OUT", urlConnection.getRequestProperties().toString());
        Log.w("Data-URL", urlbase + complement);

        urlConnection.setDoInput(true);
        urlConnection.setDoOutput(true);

        String jsonResultStr = "";

        DataOutputStream wr = new DataOutputStream(
                urlConnection.getOutputStream());
        wr.writeBytes(json);
        wr.flush();
        wr.close();


        try {
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());

            BufferedReader reader = new BufferedReader(new InputStreamReader(in));


            StringBuilder total = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                total.append(line);
            }

            jsonResultStr = total.toString();

            Log.w("Data-IN", jsonResultStr);


        } finally {
            urlConnection.disconnect();
        }


        return jsonResultStr;
    }


    /**
     * Method in use to upload a file
     *
     * @return
     * @throws IOException
     */
    public String PostToServerURL(String complement, String json, Map<String, String> params, final Map<String, String> headers, File file) throws IOException {

        String boundary = "*****";


        URL mURL = new URL(urlbase + complement);

        final HttpURLConnection urlConnection = (HttpURLConnection) mURL.openConnection();
        urlConnection.setRequestProperty("Accept", "application/json");
        for (String key: headers.keySet() ){
            urlConnection.setRequestProperty(key, headers.get(key));
        }

        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Connection", "Keep-Alive");
        urlConnection.setRequestProperty("Cache-Control", "no-cache");
        urlConnection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);


        Log.w("Data-OUT", urlConnection.getRequestProperties().toString());
        Log.w("Data-URL", urlbase + complement);

        urlConnection.setDoInput(true);
        urlConnection.setDoOutput(true);
        urlConnection.setUseCaches(false);

        String jsonResultStr = "";

        DataOutputStream wr = new DataOutputStream(
                urlConnection.getOutputStream());


        //Out put file
        String twoHyphens = "--";
        String lineEnd = "\r\n";

        String result = "";

        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;

        FileInputStream fileInputStream = new FileInputStream(file);

        bytesAvailable = fileInputStream.available();
        bufferSize = Math.min(bytesAvailable, maxBufferSize);
        buffer = new byte[bufferSize];

        bytesRead = fileInputStream.read(buffer, 0, bufferSize);

        wr.writeBytes(twoHyphens +boundary + lineEnd);
        wr.writeBytes("Content-Disposition: form-data; name=\"image\"; filename=\"jakes-bar.jpg\"");
        wr.writeBytes(lineEnd);
        wr.writeBytes("Content-Type: image/jpeg");
        wr.writeBytes(lineEnd);
        wr.writeBytes(lineEnd);
        while (bytesRead > 0) {
            wr.write(buffer, 0, bufferSize);
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        }
        wr.writeBytes(lineEnd);

        wr.writeBytes(twoHyphens +boundary + twoHyphens);

        wr.writeBytes(lineEnd);

        wr.flush();
        wr.close();


        try {
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());

            BufferedReader reader = new BufferedReader(new InputStreamReader(in));


            StringBuilder total = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                total.append(line);
            }

            jsonResultStr = total.toString();

            Log.w("Data-IN", jsonResultStr);


        } finally {
            urlConnection.disconnect();
        }


        return jsonResultStr;
    }



    private static final char PARAMETER_DELIMITER = '&';
    private static final char PARAMETER_EQUALS_CHAR = '=';

    public static String createQueryStringForParameters(Map<String, String> parameters) {
        StringBuilder parametersAsQueryString = new StringBuilder();
        if (parameters != null) {
            boolean firstParameter = true;

            for (String parameterName : parameters.keySet()) {
                if (!firstParameter) {
                    parametersAsQueryString.append(PARAMETER_DELIMITER);
                }

                parametersAsQueryString.append(parameterName)
                        .append(PARAMETER_EQUALS_CHAR)
                        .append(URLEncoder.encode(
                                parameters.get(parameterName)));

                firstParameter = false;
            }
        }
        return "?" + parametersAsQueryString.toString();
    }


}
