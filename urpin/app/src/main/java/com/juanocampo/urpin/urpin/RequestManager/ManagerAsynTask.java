package com.juanocampo.urpin.urpin.RequestManager;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by juan.ocampo
 */
public class ManagerAsynTask extends AsyncTask<String, Void, ResponseFormat> {


    public static final String POST="POST";
    public static final String GET="GET";

    private ServicesConsumer consumer;
    private String recibido;
    private JSONObject resultadoPost;
    public JSONObject resultGet;
    private String complement="";
    private Context contexto;
    private String Type= GET; //By defualt
    private ResponseFormat result= new ResponseFormat();
    private String PageLength="1";
    private Map<String,String> params= new HashMap<>();



    @Override
    protected ResponseFormat doInBackground(String... params) {
        PostMethod server = new PostMethod();
        this.Type = params[0];
        this.complement = params[1];
        int i = 2;

        if (Type.equals(GET)){
            if (params.length >= 4)
                while (params.length != i) {
                    this.params.put(params[i], params[i + 1]);
                    i = i + 2;
                }
            /*resultadoPost =*/
            try {//Get Here
                result.setData(server.GetFromServerURL(complement, this.params,null)); //
                if (result.getData().equals("")) {
                    result.setIsErrorLocal(true);
                    result.setErrorMsn("Error connection or empty response");
                }

            } catch (IOException e) {
                e.printStackTrace();
                result.setIsErrorLocal(true);
                result.setErrorMsn(e.getMessage());
            }
        }
        else if (Type.equals(POST)){
            try {
                try {//Get Here
                    i=3;
                    if (params.length >= 5)
                        while (params.length != i) {
                            this.params.put(params[i], params[i + 1]);
                            i = i + 2;
                        }
                    result.setData(server.PostToServerURL(complement,params[2],this.params,null)); //
                    if(result.getData().equals("")){
                        result.setIsErrorLocal(true);
                        result.setErrorMsn("Error connection or empty response");
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    result.setIsErrorLocal(true);
                    result.setErrorMsn(e.getMessage());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    public ManagerAsynTask(ServicesConsumer consumer, Context contexto)
    {
        this.consumer = consumer;
        this.recibido = null;
        this.resultadoPost = null;
        this.contexto = contexto;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(ResponseFormat result) {
        Log.i("Desde: ", "onPostManagerAsyn");

        consumer.processResponse(result);
        super.onPostExecute(result);
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }




}
