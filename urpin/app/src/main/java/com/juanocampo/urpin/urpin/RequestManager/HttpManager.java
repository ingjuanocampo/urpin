package com.juanocampo.urpin.urpin.RequestManager;

import android.os.AsyncTask;
import android.support.annotation.IntDef;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by juanocampo
 */
public class HttpManager {
    private Map<String, String> mParams = new HashMap<>();
    private Map<String, String> mHeaders = new HashMap<>();
    private int mMethod;
    private ServicesConsumer consumer;
    private String mRaw = "";
    private String mBodyFilePath;
    private boolean defaultHeaders = true;
    private String mFilePath ="";

    @IntDef({POST, GET, DELETE, POST_FILE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface HttpMethods {

    }

    public static final int POST = 1;
    public static final int GET = 2;
    public static final int DELETE = 3;
    public static final int POST_FILE = 4;

    public HttpManager setmHeaders(Map<String, String> mHeaders) {
        this.mHeaders = mHeaders;
        return this;
    }

    public HttpManager addHeader(String tag, String value) {
        this.mHeaders.put(tag, value);
        return this;
    }

    public HttpManager addParams(String tag, String value) {
        mParams.put(tag, value);
        return this;
    }

    public HttpManager setParams(Map<String, String> mParams) {
        this.mParams = mParams;
        return this;
    }

    public HttpManager setmMethod(@HttpMethods int mMethod) {
        this.mMethod = mMethod;
        return this;
    }

    public HttpManager setConsumer(ServicesConsumer consumer) {
        this.consumer = consumer;
        return this;
    }

    public HttpManager setmRaw(String mRaw) {
        this.mRaw = mRaw;
        return this;
    }

    public HttpManager setBody (String filePath) {
        this.mBodyFilePath = filePath;
        return this;
    }

    public boolean isDefaultHeaders() {
        return defaultHeaders;
    }

    public HttpManager setDefaultHeaders(boolean defaultHeaders) {
        this.defaultHeaders = defaultHeaders;
        return this;
    }

    public String getmFilePath() {
        return mFilePath;
    }

    public HttpManager setmFilePath(String mFilePath) {
        this.mFilePath = mFilePath;
        return this;
    }

    private ManagerAsycTask asyntasks;
    public void excute(String urlComplement) {
        //TODO add check for the necessary params here
         asyntasks.cancel(true);
         asyntasks = new ManagerAsycTask();
         asyntasks.execute(urlComplement);

    }

    private class ManagerAsycTask extends AsyncTask<String, Void, ResponseFormat> {

        @Override
        protected ResponseFormat doInBackground(String... complement) {
            ResponseFormat result = new ResponseFormat();
            PostMethod server = new PostMethod();

            if(!isCancelled()) {
                if (mMethod == GET) {
                    try {//Get Here
                        if(defaultHeaders)
                            setBasicHeaderGet();
                        result.setData(server.GetFromServerURL(complement[0], mParams, mHeaders)); //
                        if (result.getData().equals("")) {
                            result.setIsErrorLocal(true);
                            result.setErrorMsn("Error connection or empty response");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        result.setIsErrorLocal(true);
                        result.setErrorMsn(e.getMessage());
                    }
                } else if (mMethod == POST) {
                    try {
                        if(defaultHeaders)
                            setBasicHeaderPost();
                        result.setData(server.PostToServerURL(complement[0], mRaw, mParams, mHeaders)); //


                        if (result.getData().equals("")) {
                            result.setIsErrorLocal(true);
                            result.setErrorMsn("Error connection or empty response");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        result.setIsErrorLocal(true);
                        result.setErrorMsn(e.getMessage());
                    }
                } else if (mMethod ==POST_FILE) {
                    try {
                        if(defaultHeaders)
                            setBasicHeaderPost();
                        result.setData(server.PostToServerURL(complement[0], mRaw, mParams, mHeaders, new File(mFilePath))); //


                        if (result.getData().equals("")) {
                            result.setIsErrorLocal(true);
                            result.setErrorMsn("Error connection or empty response");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        result.setIsErrorLocal(true);
                        result.setErrorMsn(e.getMessage());
                    }
                }
            }
            return result;
        }

        @Override
        protected void onPostExecute(ResponseFormat responseFormat) {

            Log.i("From: ", "onPostManagerAsyn");
            consumer.processResponse(responseFormat);
            super.onPostExecute(responseFormat);
        }
    }

    private void setBasicHeaderPost() {
        mHeaders.put("Content-Type",
                "application/x-www-form-urlencoded");

    }

    private void setBasicHeaderGet() {
        mHeaders.put("Accept", "application/json");
    }

}
