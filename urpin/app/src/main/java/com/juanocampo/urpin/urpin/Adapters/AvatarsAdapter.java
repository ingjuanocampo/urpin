package com.juanocampo.urpin.urpin.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.common.collect.Lists;
import com.juanocampo.urpin.urpin.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by juan.ocampo
 */
public class AvatarsAdapter extends RecyclerView.Adapter {


    public interface AvatarAdaperAction {
        void onAvatarClick(String url);
    }

    private List<Avatar> avatars = Lists.newArrayList();
    private Context context;
    private AvatarAdaperAction listener;
    private int preSelectedAvatar;

    public AvatarsAdapter(Context context, AvatarAdaperAction listener) {
        this.context = context;
        this.listener = listener;

        this.avatars.add(new Avatar("http://goo.gl/59ZUIr"));
        this.avatars.add(new Avatar("http://goo.gl/hmn1Jj"));
        this.avatars.add(new Avatar("http://goo.gl/b8VcxK"));
        this.avatars.add(new Avatar("http://goo.gl/auS0Jj"));
        this.avatars.add(new Avatar("http://goo.gl/G6XGml"));
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AvatarsViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final AvatarsViewHolder avatarsViewHolder = (AvatarsViewHolder)holder;

        avatarsViewHolder.selected_layer.setVisibility(avatars.get(position).isSelected() ? View.VISIBLE : View.GONE);
        Picasso.with(context).
                load(avatars.get(position).getUrl())
               .placeholder(R.drawable.placeholder)
                .stableKey(avatars.get(position).getUrl())
               .into(avatarsViewHolder.avatarImage);

        avatarsViewHolder.avatarImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                avatars.get(preSelectedAvatar).setIsSelected(false);
                avatars.get(position).setIsSelected(!avatars.get(position).isSelected());

                if(avatars.get(position).isSelected()){
                    listener.onAvatarClick(avatars.get(position).getUrl());
                } else {
                    listener.onAvatarClick("");
                }

                preSelectedAvatar = position;
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return avatars.size();
    }

    private class AvatarsViewHolder extends RecyclerView.ViewHolder {

        private ImageView avatarImage;
        private View selected_layer;

        public AvatarsViewHolder(ViewGroup parent) {
            super(LayoutInflater.from(parent.getContext()).inflate(R.layout.avatar_layout, parent, false));
            avatarImage = (ImageView)itemView.findViewById(R.id.avatar_image);
            selected_layer = itemView.findViewById(R.id.select_layer);
        }
    }

    private class Avatar {
        private String url;
        private boolean isSelected;

        public Avatar(String url) {
            this.url = url;
        }

        public void setIsSelected(boolean isSelected) {
            this.isSelected = isSelected;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public String getUrl() {
            return url;
        }
    }
}
