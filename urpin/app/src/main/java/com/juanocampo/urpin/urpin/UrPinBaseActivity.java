package com.juanocampo.urpin.urpin;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.juanocampo.urpin.urpin.RequestManager.ServicesConsumer;

/**
 * Created by juan.ocampo
 */
public abstract class UrPinBaseActivity extends AppCompatActivity implements ServicesConsumer {


    protected RecyclerView mRecyclerView;
    protected ProgressBar mProgressBar;
    private static AlertDialog alert;


    protected interface BaseActions {
        void onOkDialogButtonPress();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getResourceLayout());
        findWidgetsById();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    protected abstract void findWidgetsById();


    protected abstract void fabAction(View view);


    protected abstract int getResourceLayout();


    protected void showGenericErrorMsn() {
        showDialog("Connection Problem", "We have connectivity problems, sorry ¡ :S ");
    }

    protected void showDialog(String title,String Msn){
        showDialog(title,Msn,null);
    }


        /**
         *
         * @param title
         * @param Msn
         */
    protected void showDialog(String title,String Msn, final BaseActions listner){

        if(!((Activity) this).isFinishing()){

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(title);
            builder.setMessage(Msn)
                   .setCancelable(false)
                   .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                       public void onClick(DialogInterface dialog, int id) {
                           if(listner!=null)
                            listner.onOkDialogButtonPress();
                       }
                   });
            if(alert!=null){
                if(!alert.isShowing()) {
                    alert = builder.create();
                    alert.show();
                }
            }else {
                alert = builder.create();
                alert.show();
            }

        }

    }

}
