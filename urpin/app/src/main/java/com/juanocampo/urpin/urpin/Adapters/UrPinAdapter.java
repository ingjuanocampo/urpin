package com.juanocampo.urpin.urpin.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.common.collect.Lists;
import com.juanocampo.urpin.urpin.Model.UrPin;
import com.juanocampo.urpin.urpin.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by juanocampo on 11/24/15.
 */
public class UrPinAdapter extends RecyclerView.Adapter {

    public interface UrPinAdapterActions {
        void onUrPinClick (UrPin urPin);
    }

    private List<UrPin> urPins = Lists.newArrayList();
    private Context context;
    private UrPinAdapterActions listener;

    public UrPinAdapter(List<UrPin> urPins, Context context, UrPinAdapterActions urPinAdapterActions) {
        this.urPins = urPins;
        this.context = context;
        this.listener = urPinAdapterActions;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new UrPinViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        UrPinViewHolder urPinViewHolder = (UrPinViewHolder) holder;
        urPinViewHolder.urPinText.setText(urPins.get(position).getUrpinName());

        if(!urPins.get(position).getUrlPhotoProfile().equals(null) && !urPins.get(position).getUrlPhotoProfile().equals("")){
            Picasso.with(context).
                    load(urPins.get(position).getUrlPhotoProfile())
                    .stableKey(urPins.get(position).getUrlPhotoProfile())
                   .placeholder(R.drawable.placeholder)
                   .into(urPinViewHolder.urPinImage);
        }


        urPinViewHolder.urPinImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onUrPinClick(urPins.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.urPins.size();
    }


    public class UrPinViewHolder extends RecyclerView.ViewHolder {

        public TextView urPinText;
        public ImageView urPinImage;

        public UrPinViewHolder(ViewGroup parent) {
            super(LayoutInflater.from(parent.getContext()).inflate(R.layout.ur_pin_item, parent, false));

            urPinImage = (ImageView)itemView.findViewById(R.id.ur_pin_photo);
            urPinText = (TextView)itemView.findViewById(R.id.ur_pin_name);


        }
    }


}
