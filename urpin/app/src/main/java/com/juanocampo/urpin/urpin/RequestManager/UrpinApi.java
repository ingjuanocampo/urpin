package com.juanocampo.urpin.urpin.RequestManager;

/**
 * Created by juan.ocampo on 05/02/2016.
 */
public class UrpinApi {
    private final HttpManager manager;

    public UrpinApi(HttpManager manager) {
        this.manager = manager;
    }

    public interface UrPinApiResponse {
        void onSucess();
        void onError();
    }


    public void getProfile(final UrPinApiResponse response){
        manager.addHeader("","").setConsumer(new ServicesConsumer() {
            @Override
            public void processResponse(ResponseFormat json) {
                if(json.isErrorLocal())
                    response.onError();
                else
                    response.onSucess();
            }
        }).excute("/profile");
    }

}
