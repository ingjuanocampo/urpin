package com.juanocampo.urpin.urpin;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.common.collect.Lists;
import com.juanocampo.urpin.urpin.Adapters.AvatarsAdapter;
import com.juanocampo.urpin.urpin.RequestManager.HttpManager;
import com.juanocampo.urpin.urpin.RequestManager.ResponseFormat;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

/**
 * Created by juan.ocampo
 */
public class UpLoadNewUrPin extends UrPinBaseActivity implements AvatarsAdapter.AvatarAdaperAction {


    private TextView urPinName;
    private TextView urPinFeature;
    private TextView urPinUrlPicture;
    private Spinner urPinRegionSpinner;
    private static final String PARAMETER_SEPARATOR = "&";
    private String avatarUrl;

    public static int UPLOAD_NEW_URPIN = 991;


    @Override
    protected void findWidgetsById() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fabAction(view);
            }
        });

        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);

        urPinName = (TextView) findViewById(R.id.ur_pin_name);
        urPinFeature = (TextView) findViewById(R.id.ur_pin_feature);
        urPinUrlPicture = (TextView) findViewById(R.id.ur_pin_photo);
        urPinRegionSpinner = (Spinner) findViewById(R.id.ur_region);


        List<String> regionOptions = Lists.newArrayList();
        regionOptions.add("1");

        urPinRegionSpinner.setAdapter(new ArrayAdapter(this, android.R.layout.simple_spinner_item, regionOptions));

        RecyclerView avatarList = (RecyclerView)findViewById(R.id.recycler_view);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        avatarList.setLayoutManager(manager);
        avatarList.setAdapter(new AvatarsAdapter(this, this));

        findViewById(R.id.info_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog("Information", "You should introduce a short URL because the services doesn't support large URLs");
            }
        });

        findViewById(R.id.info_region).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog("Information", "This is a Region that I created, for that reason is the only available");
            }
        });

    }

    @Override
    protected void fabAction(View view) {
        showProgressBar();
        HttpManager httpManager = new HttpManager();
        httpManager.setmMethod(HttpManager.POST)
                   .setmRaw(getParams())
                   .setConsumer(this)
                   .excute("/insertGrid");
    }

    private String getParams() {
        String parameters = null;
        try {
            String photoUrl = !avatarUrl.equals("") ? avatarUrl : URLEncoder.encode(urPinUrlPicture.getText().toString(), "UTF-8");

            parameters = "urpinName=" + URLEncoder.encode(urPinName.getText().toString(), "UTF-8");
            parameters = parameters + PARAMETER_SEPARATOR + "feature=" + URLEncoder.encode(urPinFeature.getText().toString(), "UTF-8");
            parameters = parameters + PARAMETER_SEPARATOR + "urlPhotoProfile=" + photoUrl;
            parameters = parameters + PARAMETER_SEPARATOR + "idRegion=" + URLEncoder.encode((String) urPinRegionSpinner.getSelectedItem(), "UTF-8");

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return parameters;
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.upload_urpin_activity;
    }

    @Override
    public void processResponse(ResponseFormat responseFormat) {
        hideProgressbar();
        if (!responseFormat.isErrorLocal() && !responseFormat.isErrorServer()) {
            showDialog("Success", "You new UrPin has been uploaded", new BaseActions() {
                @Override
                public void onOkDialogButtonPress() {
                    setResult(UPLOAD_NEW_URPIN);
                    finish();
                }
            });
        } else {
            showGenericErrorMsn();
        }
    }

    private void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
        findViewById(R.id.upload_container).setVisibility(View.GONE);
    }

    private void hideProgressbar() {
        mProgressBar.setVisibility(View.GONE);
        findViewById(R.id.upload_container).setVisibility(View.VISIBLE);
    }

    @Override
    public void onAvatarClick(String url) {
        this.avatarUrl = url;
        urPinUrlPicture.setText(url);
    }
}
