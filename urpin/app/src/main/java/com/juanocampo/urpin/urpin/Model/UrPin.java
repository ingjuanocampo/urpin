package com.juanocampo.urpin.urpin.Model;

/**
 * Created by juanocampo
 */
public class UrPin {

    private String urpinName;
    private String feature;
    private String urlPhotoProfile;
    private String idRegion;

    public UrPin(String urpinName, String feature, String urlPhotoProfile, String idRegion) {
        this.urpinName = urpinName;
        this.feature = feature;
        this.urlPhotoProfile = urlPhotoProfile;
        this.idRegion = idRegion;
    }

    public String getUrpinName() {
        return urpinName;
    }

    public String getFeature() {
        return feature;
    }

    public String getUrlPhotoProfile() {
        return urlPhotoProfile;
    }

    public String getIdRegion() {
        return idRegion;
    }

}
