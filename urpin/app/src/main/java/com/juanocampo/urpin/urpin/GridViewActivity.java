package com.juanocampo.urpin.urpin;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.juanocampo.urpin.urpin.Adapters.UrPinAdapter;
import com.juanocampo.urpin.urpin.Model.UrPin;
import com.juanocampo.urpin.urpin.RequestManager.HttpManager;
import com.juanocampo.urpin.urpin.RequestManager.ResponseFormat;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * @author juan.ocampo
 */

public class GridViewActivity extends UrPinBaseActivity implements UrPinAdapter.UrPinAdapterActions {

    private final static int SPAN_COUNT = 4;

    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public void findWidgetsById() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fabAction(view);
            }
        });

        mProgressBar = (ProgressBar)findViewById(R.id.progress_bar);
        mRecyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        GridLayoutManager manager = new GridLayoutManager(this, SPAN_COUNT);
        mRecyclerView.setLayoutManager(manager);
        getUrPinsFromServer();

        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.activity_main_swipe_refresh_layout);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getUrPinsFromServer();
            }
        });

    }

    @Override
    protected void fabAction(View view) {
        startActivityForResult(new Intent(this, UpLoadNewUrPin.class), 0);
    }

    private void getUrPinsFromServer() {
        showProgressBar();

        HttpManager httpManager = new HttpManager();
        httpManager.setmMethod(HttpManager.GET)
                .addParams("amount", "100") // Be aware this isn't a good practice, the serve should allow us ask for a specific page (Should have
                // a pages system)
                .addParams("region", "1") //This is the simple region that I created for this test.
                .setConsumer(this)
        .excute("/getGrid");
    }

    @Override
    public void processResponse(ResponseFormat responseFormat) {
        swipeRefreshLayout.setRefreshing(false);
        hideProgressbar();
        if (!responseFormat.isErrorLocal() && !responseFormat.isErrorServer()) {
            mRecyclerView.setAdapter(new UrPinAdapter(parseUrPin(responseFormat.getData()), this, this));
        } else {
            showGenericErrorMsn();
        }
    }

    private void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.GONE);
    }

    private void hideProgressbar() {
        mProgressBar.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    private ArrayList<UrPin> parseUrPin (String urpinString) {
            Gson gson = new Gson();
            Type token = new TypeToken<ArrayList<UrPin>>(){}.getType();
            return gson.fromJson(urpinString, token);
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.activity_main;
    }

    @Override
    public void onUrPinClick(UrPin urPin) {
        showDialog("Feature", urPin.getFeature());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == UpLoadNewUrPin.UPLOAD_NEW_URPIN) {
            getUrPinsFromServer();
        }

    }
}
